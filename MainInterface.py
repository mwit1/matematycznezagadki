import tkinter as tk
from tkinter import simpledialog
import tkinter.ttk as ttk
import random

from QuestionEasy import question_easy
from QuestionMedium import question_medium
from QuestionHard import question_hard


class MatematyczneZagadkiApp:
    def __init__(self, root):
        self.root = root
        self.root.geometry("1920x1080")
        self.root.title("Matematyczne Zagadki")
        self.root.configure(bg="sky blue")

        self.user_nickname = ""
        self.default_text = "Wpisz swoją nazwę"
        self.user_points = 0
        self.results = []
        self.nickname_entered = False
        self.user_label = tk.Label(self.root, text="", font=('Arial', 24), bg='sky blue')
        self.user_label.pack(pady=20)
        self.answer_checked = False

        self.load_results_from_file()
        self.create_main_menu()

    def create_main_menu(self): #interfejs menu
        self.clear_window()

        label = tk.Label(self.root, text="Matematyczne zagadki", bg='sky blue', font=('Arial', 30,'bold'))
        label.pack(padx=20, pady=80)

        buttonPlay = tk.Button(self.root, text="Graj", width=30, bg='lightblue', font=('Arial', 26),
                               command=self.play_game)
        buttonRules = tk.Button(self.root, text="Zasady gry", width=30, bg='lightblue', font=('Arial', 26),
                                command=self.rules)
        buttonHistory = tk.Button(self.root, text='Najlepsze gry', width=30, bg='lightblue', font=('Arial', 26),
                                  command=self.show_top_10_games)
        buttonPlay.pack(pady=15)
        buttonRules.pack(pady=15)
        buttonHistory.pack(pady=15)

        buttonExit = tk.Button(self.root, text="Wyjdź z gry", width=30, bg='lightblue', font=('Arial', 26),
                               command=self.root.destroy)
        buttonExit.pack(pady=15)

    def get_user_and_play(self): #sprawdzanie nicku użytkownika
        self.nickname_entered = True
        self.user_nickname = self.get_user_nickname()
        self.play_game()

    def get_user_nickname(self): #użytkownik podaje nick

        nickname = simpledialog.askstring("Nick", "Podaj swój nick:")
        return nickname if nickname else "Anonim"

    def show_top_10_games(self): #interfejs z wynikami
        self.clear_window()
        najlepsze_gry = tk.Label(self.root, text='Top 10 wyników:', justify='center', fg='black',
                                 font=('Arial', 32, 'bold'), bg='sky blue')
        najlepsze_gry.pack(pady=20)

        columns = ("Nick", "Punkty")
        tree = ttk.Treeview(self.root, columns=columns, show="headings", selectmode="none")
        tree.heading("Nick", text="Nick")
        tree.heading("Punkty", text="Punkty")
        tree.column("Nick", width=150)
        tree.column("Punkty", width=100)
        tree.pack(pady=20)

        sorted_results = bubble_sort(self.results)[:10]
        for result in sorted_results:
            tree.insert("", "end", values=result)

        print("Aktualna lista wyników:", self.results)

        back_to_menu_button = tk.Button(self.root, text="Powrót do menu głównego", font=('Arial', 16),
                                        bg='lightblue', command=self.create_main_menu)
        back_to_menu_button.pack(pady=20)

        self.root.update_idletasks()
        width = self.root.winfo_width()
        height = self.root.winfo_height()
        x = (self.root.winfo_screenwidth() - width) // 2
        y = (self.root.winfo_screenheight() - height) // 2
        self.root.geometry(f"{width}x{height}+{x}+{y}")

    def rules(self): #interfejs z zasadami
        self.clear_window()
        zasady = tk.Label(self.root, text='Zasady gry:', font=('Arial', 32, 'bold'), justify='center', bg='sky blue')
        zasady.pack(pady=50)
        rule1 = tk.Label(self.root, text='1. Wybierz pytanie z jednej z 3 pul: pytań łatwych, średnich i trudnych.',
                         font=('Arial', 20), justify='center', bg='sky blue')
        rule2 = tk.Label(self.root,
                         text='2. Za pytanie łatwe gracz dostaje 1 punkt, za pytanie średnie 2 punkty, za pytanie trudne 3 punkty.',
                         font=('Arial', 20), justify='center', bg='sky blue')
        rule3 = tk.Label(self.root,
                         text='3. Uzbieraj jak najwięcej punktów. Po udzieleniu złej odpowiedzi punkty się resetują.',
                         font=('Arial', 20), justify='center', bg='sky blue')
        rule4 = tk.Label(self.root,
                         text='4. Najlepsze 10 wyników pokaże się w tabeli po kliknięciu "Najlepsze gry" w menu głównym.',
                         font=('Arial', 20), justify='center', bg='sky blue')
        back_to_menu_button = tk.Button(self.root, text="Powrót do menu głównego", font=('Arial', 16),
                                        bg='lightblue', command=self.create_main_menu)

        rule1.pack(pady=10)
        rule2.pack(pady=10)
        rule3.pack(pady=10)
        rule4.pack(pady=10)
        back_to_menu_button.pack(pady=80)

    def play_game(self): #interfejs z wyborem pytań

        if not self.nickname_entered:
            self.user_nickname = self.get_user_nickname()
            self.nickname_entered = True

        self.clear_window()

        self.user_label = tk.Label(self.root, text=f"Witaj, {self.user_nickname}! Punkty: {self.user_points}",
                                   font=('Arial', 24), bg='sky blue')
        self.user_label.pack(pady=20)

        level = tk.Label(self.root, text='Wybierz poziom trudności pytania:', justify='center', fg='black',
                         font=('Arial', 32, 'bold'), bg='sky blue')
        level.pack(pady=40)

        buttonEasy = tk.Button(self.root, text="Łatwy (+1 punkt)", width=30, bg='lightblue', font=('Arial', 26),
                               command=self.easy_question)
        buttonMedium = tk.Button(self.root, text="Średni (+2 punkty)", width=30, bg='lightblue', font=('Arial', 26),
                                 command=self.medium_question)
        buttonHard = tk.Button(self.root, text="Trudny (+3 punkty)", width=30, bg='lightblue', font=('Arial', 26),
                               command=self.hard_question)

        buttonEasy.pack(pady=20)
        buttonMedium.pack(pady=20)
        buttonHard.pack(pady=20)

        back_to_menu_button = tk.Button(self.root, text="Powrót do menu głównego", font=('Arial', 16), bg='lightblue',
                                        command=self.create_main_menu)
        back_to_menu_button.pack(pady=20)

    def clear_window(self): #czyszczenie okna
        for widget in self.root.winfo_children():
            widget.destroy()

    def easy_question(self): #ekran po wyborze łatwego pytania
        self.clear_window()

        random_question = random.choice(question_easy)
        question_text = list(random_question.keys())[0]
        correct_answer = random_question[question_text]

        self.answer_checked = False

        self.display_question(question_text, correct_answer, 1)

    def medium_question(self): #ekran po wyborze średniego pytania
        self.clear_window()

        random_question = random.choice(question_medium)
        question_text = list(random_question.keys())[0]
        correct_answer = random_question[question_text]

        self.answer_checked = False

        self.display_question(question_text, correct_answer, 2)

    def hard_question(self): #ekran po wyborze trudnego pytania
        self.clear_window()

        random_question = random.choice(question_hard)
        question_text = list(random_question.keys())[0]
        correct_answer = random_question[question_text]

        self.answer_checked = False

        self.display_question(question_text, correct_answer, 3)

    def display_question(self, question_text, correct_answer, points): #mechanizm zliczania punktów + wyświetlanie pytań
        question_label = tk.Label(self.root, text=question_text, justify='center', fg='black', font=('Arial', 20),
                                  bg='sky blue')
        question_label.pack(pady=20)

        answer_entry = tk.Entry(self.root, width=30, justify='center', font=('Arial', 18))
        answer_entry.pack(pady=20)

        def check_answer(): #sprawdzanie poprawności odpowiedzi
            nonlocal correct_answer
            nonlocal points
            nonlocal result_label
            if not self.answer_checked:
                user_answer = answer_entry.get()
                try:
                    user_answer = int(user_answer)
                    if user_answer == correct_answer:
                        result_label.config(text="Poprawna odpowiedź!", fg='green')
                        self.user_points += points
                    else:
                        result_label.config(text="Błędna odpowiedź!", fg='red')
                        self.show_score_and_back_to_menu()
                except ValueError:
                    result_label.config(text="Wprowadź liczbę całkowitą!", fg='red')

                self.answer_checked = True

        check_button = tk.Button(self.root, text="Sprawdź odpowiedź", width=30, bg='lightblue', font=('Arial', 16),
                                 command=check_answer)
        check_button.pack(pady=20)

        result_label = tk.Label(self.root, text="", font=('Arial', 18), bg='sky blue')
        result_label.pack(pady=20)

        back_to_question_button = tk.Button(self.root, text="Powrót do wyboru pytania", font=('Arial', 16),
                                            bg='lightblue', command=self.play_game)
        back_to_question_button.pack(pady=20)

        self.user_label.config(text=f"Witaj, {self.user_nickname}! Punkty: {self.user_points}")

    def save_result(self): #zapisywanie wyników i sposób wyświetlania
        result = (self.user_nickname, self.user_points)
        self.results.append((self.user_nickname, self.user_points))

        n = len(self.results)
        for i in range(n):
            for j in range(0, n-i-1):
                if self.results[j][1] < self.results[j+1][1]:
                    self.results[j], self.results[j+1] = self.results[j+1], self.results[j]

        self.results = self.results[:10]
        print("Zapisano wynik:", result, "Nowa lista wyników:", self.results)
        self.save_results_to_file()
    def show_score_and_back_to_menu(self): #plansza po przegraniu gry
        score_label = tk.Label(self.root, text=f"Twój wynik: {self.user_points} punktów", font=('Arial', 24),
                               bg='sky blue')
        score_label.pack(pady=20)

        self.save_result()
        self.user_points = 0

        back_to_menu_button = tk.Button(self.root, text="Powrót do menu głównego", font=('Arial', 16), bg='lightblue',
                                        command=self.create_main_menu)
        back_to_menu_button.pack(pady=20)

    def save_results_to_file(self): #zapis do pliku
        with open('results.py', 'w') as file:
            file.write(f'results = {self.results}\n')

    def load_results_from_file(self): #odczyt do pliku
        try:
            with open('results.py', 'r') as file:
                code = file.read()
                exec(code, globals())
                self.results = results
        except FileNotFoundError:
            self.results = []
def bubble_sort(results): #sortowanie bąbelkowe
    n = len(results)
    for i in range(n):
        for j in range(0, n - i - 1):
            if results[j][1] < results[j + 1][1]:
                results[j], results[j + 1] = results[j + 1], results[j]

    return results


if __name__ == "__main__":
    root = tk.Tk()
    app = MatematyczneZagadkiApp(root)
    root.mainloop()
