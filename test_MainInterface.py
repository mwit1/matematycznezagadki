from MainInterface import bubble_sort
def test_bubble_sort():
    test_results = [("Gracz1", 15), ("Gracz2", 20), ("Gracz3", 10), ("Gracz4", 25), ("Gracz5", 18)]
    expected_result = [("Gracz4", 25), ("Gracz2", 20), ("Gracz5", 18), ("Gracz1", 15), ("Gracz3", 10)]
    sorted_results = bubble_sort(test_results)
    assert sorted_results == expected_result

    print("Test sortowania bąbelkowego zakończony pomyślnie!")
test_bubble_sort()
